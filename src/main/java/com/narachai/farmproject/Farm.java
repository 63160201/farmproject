/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.farmproject;

/**
 *
 * @author ASUS
 */
public class Farm {
    String name;
    double investment;
    int chicken = 0;
    double income = 0;
    double profit = 0;
    double sum = 0;
    
    Farm(String name, double investment){
        this.name = name;
        this.investment = investment;
    }
    
    void buyChicken(int chicken ,double money) {
        if(money > this.investment){
            System.out.println("Error:your money is not enough!!!");
            return;
        }
        if(money < 0){
            System.out.println("Error:money < 0!!!");
            return;
        }
        if(chicken < 0){
            System.out.println("Error:Chicken < 0!!!");
            return;
        }
        this.chicken+=chicken;
        this.investment-=money;
        this.sum+=money;
    }
    void sellChicken(int chicken ,double money) {
        if(money < 0){
            System.out.println("Error:money < 0!!!");
            return;
        }
        if(chicken > this.chicken){
            System.out.println("Error:you don't have enough chicken!!!");
            return;
        }
        if(chicken < 0){
            System.out.println("Error:Chicken < 0!!!");
            return;
        }
        this.chicken-=chicken;
        this.income+=money;
        this.profit+=this.income-this.sum;
    }
    
    void buyChickenfood(double money) {
        if(money < 0){
            System.out.println("Error:money < 0!!!");
            return;
        }
        if(money > this.investment){
            System.out.println("Error:your money is not enough!!!");
            return;
        }
        this.investment-=money;
        this.sum+=money;
    }
    
    public void print() {
        System.out.println("Name = "+this.name+"\ninvestment ="+this.investment+
                "\nChicken = "+this.chicken+"\nincome = "+this.income+"\nprofit = "
                +this.profit+"\n****************************");
    }
    
    
}
